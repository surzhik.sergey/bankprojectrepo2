package com.anonymousClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Duck duck = new Duck();

        Swimable swimable = new Swimable() {
            @Override
            public void swim() {
                System.out.println("Base implamentation");
            }
        };

        swimable.swim();
        ArrayList<String> arrayList = new ArrayList<>();
        Collections.sort(arrayList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.parseInt(String.valueOf(o1.equalsIgnoreCase(o2)));
            }
        });
    }

}
