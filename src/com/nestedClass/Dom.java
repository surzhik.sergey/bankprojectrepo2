package com.nestedClass;

public class Dom {
    private static int amountOfLevel;
    private boolean isElevatorPresent;
    private  int amountOfFlats;
//    private Address address;

    static {
        amountOfLevel = 300;
//        address = new Address("lljsdcf", 85);
        System.out.println("Static Block inizialition " + amountOfLevel);
    }

    {
        amountOfFlats = 300;
//        address = new Address("lljsdcf", 85);
        System.out.println("Block inizialition " + amountOfFlats);
    }


    public Dom(int amountOfLevel, boolean isElevatorPresent, int amountOfFlats) {
        System.out.println("Constructor" + amountOfFlats);
        this.amountOfLevel = amountOfLevel;
        this.isElevatorPresent = isElevatorPresent;
        this.amountOfFlats = amountOfFlats;
    }

//    public Address getAddress() {
//        return address;
//    }
//
//    public void setAddress(Address address) {
//        this.address = address;
//    }

    public int getAmountOfLevel() {
        return amountOfLevel;
    }

    public boolean isElevatorPresent() {
        return isElevatorPresent;
    }

    public int getAmountOfFlats() {
        return amountOfFlats;
    }

    private static  void openDor()
    {
        System.out.println("Дверь открыта");
    }

    @Override
    public String toString() {
        return "Dom{" +
                "amountOfLevel=" + amountOfLevel +
                ", isElevatorPresent=" + isElevatorPresent +
                ", amountOfFlats=" + amountOfFlats +
//                ", address=" + address +
                '}';
    }

//    public class Address
//    {
//        private String ulica;
//        private int nomerDoma;
//
//        public Address(String ulica, int nomerDoma) {
//            this.ulica = ulica;
//            this.nomerDoma = nomerDoma;
//        }
//
//        public void getUlica()
//        {
//            openDor();
//        }
//
//        @Override
//        public String toString() {
//            return "Address{" +
//                    "ulica='" + ulica + '\'' +
//                    ", nomerDoma=" + nomerDoma +
//                    "Number of Level=" + amountOfLevel + '}';
//        }
//    }
}
