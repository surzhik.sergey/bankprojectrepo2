package com.localMethodClass;

public class PhoneNumberValidator {

   public void validatePhoneNumber(String number) {

        class PhoneNumber {

           private String phoneNumber;

           public PhoneNumber() {
               this.phoneNumber = number;
           }

           public String getPhoneNumber() {
               return phoneNumber;
           }

           public void setPhoneNumber(String phoneNumber) {
               this.phoneNumber = phoneNumber;
           }
       }

       PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(number);
       System.out.println("Nome valid: " + phoneNumber.getPhoneNumber());
       //...код валидации номера
   }

   public void otherValidate(String number)
   {

   }
}