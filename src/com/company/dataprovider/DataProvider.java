package com.company.dataprovider;

import com.company.enums.CurrencyType;
import com.company.implementation.BankAccount;

public interface DataProvider {
    String getHolderName();
    long getAccountNumber();
    CurrencyType getCurrency();
    double getBalance();
    void withdrawalMoney(BankAccount bankAccount);
}
