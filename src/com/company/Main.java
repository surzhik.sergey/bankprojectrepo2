package com.company;

import com.company.dataprovider.ConsoleDataProvider;
import com.company.dataprovider.DataProvider;
import com.company.enums.CurrencyType;
import com.company.implementation.BankAccount;

public class Main {
    public static void main(String[] args) {
        DataProvider consoleDataProvider = new ConsoleDataProvider();
        final String holderName = consoleDataProvider.getHolderName();
        final long accountNumber = consoleDataProvider.getAccountNumber();
        final CurrencyType currency = consoleDataProvider.getCurrency();
        final double balance = consoleDataProvider.getBalance();
        BankAccount myAccount = new BankAccount(holderName, accountNumber, currency, balance);
        consoleDataProvider.withdrawalMoney(myAccount);
    }

}
