package com.company.validator;

public class HolderNameValidator implements Validator<String> {
    @Override
    public boolean isValid(String inputData) {
        return inputData.length() < 5;
    }
}
