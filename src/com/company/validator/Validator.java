package com.company.validator;

import com.company.exceptions.IncorrectlyNameException;

public interface Validator<T> {
    boolean isValid(T inputData) throws IncorrectlyNameException;
}
