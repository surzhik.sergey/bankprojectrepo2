package com.company.implementation;

import com.company.enums.CurrencyType;
import com.company.exceptions.AccountNumberMinusException;
import com.company.exceptions.BalanceMinusException;
import com.company.exceptions.IncorrectlyNameException;
import com.company.exceptions.InvalidCurrencyException;

import java.util.Date;
import java.util.Scanner;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;

public class BankAccount {
    private long accountNumber;
    private CurrencyType currency;
    private double balance;
    private Date date;
    private String userName;


    public BankAccount(String userName, long accountNumber, CurrencyType currency, double balance)
    {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.currency = currency;
        this.date = new Date();
        this.userName = userName;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public double getBalance() {
        return balance;
    }

    public Date getDate() {
        return date;
    }

    public String getUserName() {
        return userName;
    }

    public boolean getMoney(double sum) throws BalanceMinusException {
        if (balance < sum)
            throw new BalanceMinusException(balance, sum);
        balance -= sum;
        System.out.println("Your balance: " + balance);
        return true;
    }


    @Override
    public String toString() {
        return "Name =" + "\t" + userName + "\n" +
                "Account number =" + "\t" + accountNumber + "\n" +
                "Balance =" + "\t" + balance + "\n" +
                "Currency =" + currency + "\n" +
                "date =" + date();
    }


}



