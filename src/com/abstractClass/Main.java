package com.abstractClass;

public class Main {
    public static void main(String[] args) {
        LegcovoyCarAbstract legkovaya = new LegcovoyCarAbstract();
        CarAbstract gruzovaya = new GruzovayaCarAbstract();
        legkovaya.setCarName("Test legkovaya");
        legkovaya.setCountOfSeat(4);
        gruzovaya.setCarName("Test gruzovaya");
        legkovaya.openDor();
        gruzovaya.openDor();
        System.out.println(legkovaya);
        System.out.println(gruzovaya);
    }
}
