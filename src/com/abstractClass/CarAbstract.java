package com.abstractClass;

public abstract class CarAbstract {
    private String carName;

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarName() {
        return carName;
    }

    public abstract void gaz();

    public  abstract void tormoz();

    public void openDor()
    {
        System.out.println("Я машина и я у меня открывать двери");
    }

    @Override
    public String toString() {
        return "Car{" +
                "carName='" + carName + '\'' +
                '}';
    }
}
