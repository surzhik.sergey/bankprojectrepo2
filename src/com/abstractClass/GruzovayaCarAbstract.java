package com.abstractClass;

public class GruzovayaCarAbstract extends CarAbstract {
    @Override
    public void gaz() {
        System.out.println("Я грузовая машина и я газую");
    }

    @Override
    public void tormoz() {
        System.out.println("Я грузовая машина и я торможу");
    }
}
