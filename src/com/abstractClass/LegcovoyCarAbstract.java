package com.abstractClass;

public class LegcovoyCarAbstract extends CarAbstract {
    @Override
    public String toString() {
        return "LegcovoyCarAbstract{" +
                "Car name=" + getCarName() +
                "countOfSeat=" + countOfSeat +
                '}';
    }

    @Override
    public void openDor()
    {
        System.out.println("Я легковая машина и я имею двери");
    }

    public int getCountOfSeat() {
        return countOfSeat;
    }

    public void setCountOfSeat(int countOfSeat) {
        this.countOfSeat = countOfSeat;
    }

    private int countOfSeat;

    @Override
    public void gaz() {
        System.out.println("Я легковая машина и я газую");
    }

    @Override
    public void tormoz() {
        System.out.println("Я легковая машина и я торможу");
    }
}
